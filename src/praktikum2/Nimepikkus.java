package praktikum2;

import praktikum1.TextIO;

public class Nimepikkus {

	public static void main(String[] args) {
         
		String nimi;
		
		System.out.println("Mis on su nimi?");
		nimi = TextIO.getlnString();
		System.out.println("Sinu nime pikkus on " + nimi.length() + " tähte.");
	
	}

}
