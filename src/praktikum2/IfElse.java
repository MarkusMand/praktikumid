package praktikum2;

import praktikum1.TextIO;

public class IfElse {

	 public static void main(String[] args) {

	        int testscore;
	        char grade;

	        System.out.println("Mitu punkti te saite?");
	        testscore = TextIO.getlnInt();
	        
	        if (testscore >= 90) {
	            grade = '5';
	        } else if (testscore >= 80) {
	            grade = '4';
	        } else if (testscore >= 70) {
	            grade = '3';
	        } else if (testscore >= 60) {
	            grade = '2';
	        } else {
	            grade = '1';
	        }
	        System.out.println("Siis teie hinne on " + grade);
	    }
	}