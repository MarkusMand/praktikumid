package praktikum2;

import praktikum1.TextIO;

public class Asendus {

	public static void main(String[] args) {
		
		String text;
		
		System.out.print("Mis täna teed?");
		text = TextIO.getlnString();
		System.out.println("Sinu tekst on siis: ");
		System.out.println(text.replace("a", "_").replace("A", "_"));
		

	}

}
