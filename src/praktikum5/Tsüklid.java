package praktikum5;

public class Tsüklid {

	public static void main(String[] args) {
		
		if (true ) {
		System.out.println("laused, mis täidetakse, kui tingimus on tõene!");
		
		}
		int j = 10;
		while (j > 0) {
			System.out.println("laused, mis täidetakse, kui tingimus on tõene!");
			System.out.println("i väärtus on: " + j);
			break;
		}
		
		for (int i = 0; i < 10 ; i++) {
			System.out.println("for tsükkel, i väärtus" + i);
			
		}
	}

}
